#!/bin/zsh

export TERM="xterm-256color"
export PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:~/.local/bin:$PATH"
export EDITOR='nvim'
export CLICOLOR=1
export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=5'

export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

export PATH="$PATH:/home/czyzc/.local/bin"

# ZSH History
HISTFILE=$HOME/.zsh_history
SAVEHIST=10000                              # Big history
HISTSIZE=10000
setopt SHARE_HISTORY                        # Shares history across multiple zsh sessions, in real time
setopt HIST_IGNORE_DUPS                     # Do not write events to history that are duplicates of the immediately previous event
setopt INC_APPEND_HISTORY                   # Add commands to history as they are typed, don't wait until shell exit
setopt HIST_REDUCE_BLANKS                   # Remove extra blanks from each command line being added to history
setopt hist_verify
setopt hist_ignore_all_dups
setopt hist_expire_dups_first
setopt hist_save_no_dups

[ -f ~/.zsh_aliases ] && source ~/.zsh_aliases
[ -f ~/.zsh_env ] && . ~/.zsh_env

fpath=(~/.zsh $fpath)

# Uncomment the following line to change how often to auto-update (in days).
export UPDATE_ZSH_DAYS=5

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

[[ -d /home/linuxbrew/.linuxbrew && $- == *i* ]] && eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
if [ -f $(brew --prefix)/etc/brew-wrap ];then
  source $(brew --prefix)/etc/brew-wrap
fi
# source antidote
# git clone --depth=1 https://github.com/mattmc3/antidote.git ${ZDOTDIR:-~}/.antidote
source ${ZDOTDIR:-~}/.antidote/antidote.zsh


# initialize plugins statically with ${ZDOTDIR:-~}/.zsh_plugins.txt
antidote load

if command -v starship 1>/dev/null 2>&1; then eval "$(starship init zsh)"; fi
autoload -Uz compinit promptinit
promptinit
compinit

# Zoxide
if command -v zoxide 1>/dev/null 2>&1; then eval "$(zoxide init zsh)"; fi
# Direnv hook
if command -v direnv 1>/dev/null 2>&1; then eval "$(direnv hook zsh)"; fi

# Pyenv hook
export PATH="$HOME/.pyenv/bin:$PATH"
if command -v pyenv 1>/dev/null 2>&1; then eval "$(pyenv init -)"; fi

# Python
export PIP_REQUIRE_VIRTUALENV=true

# TheFuck
if command -v fuck 1>/dev/null 2>&1; then eval "$(thefuck --alias)"; fi

# FZF
source <(fzf --zsh)
[ -f /usr/share/fzf/key-bindings.zsh ] && source /usr/share/fzf/key-bindings.zsh
[ -f /usr/share/fzf/completion.zsh ] && source /usr/share/fzf/completion.zsh

# Use bat for preview
export FZF_COMPLETION_OPTS="--preview 'bat --style=numbers --color=always {}'"

# Use fd instead of find
_fzf_compgen_path() {
	fd --hidden --follow --exclude ".git" --exclude ".direnv" . "$1"
}
_fzf_compgen_dir() {
	fd --type d --hidden --follow --exclude ".git" --exclude ".direnv" . "$1"
}

# END

# Created by `pipx` on 2024-01-28 23:14:55
