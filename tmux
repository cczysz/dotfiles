# Tmux conf
set -s escape-time 10 
set -g prefix C-a                        # GNU-Screen compatible prefix
set -g mouse on

bind r source-file ~/.tmux.conf \; display '~/.tmux.conf sourced'

set -g base-index 1           # start windows numbering at 1
setw -g pane-base-index 1     # make pane numbering consistent with windows

setw -g automatic-rename on   # rename window to reflect current program
set -g renumber-windows on    # renumber windows when a window is closed


# Plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'christoomey/vim-tmux-navigator'
set -g @plugin 'joshmedeski/t-smart-tmux-session-manager'

# Fix vim navigator overwrite
bind C-l send-keys 'C-l' # prefix C-l to clear

if-shell "test -f ~/.tmuxline.conf" "source ~/.tmuxline.conf"
# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run -b '~/.tmux/plugins/tpm/tpm'
