"====================== General Vim Settings =========================

filetype plugin on     " required!
filetype indent on
syntax on

" File settings
set autoread
set updatetime=100

set encoding=utf8

set noswapfile
set nobackup
set nowb

if !has('gui_running')
  set t_Co=256
endif

" Map Leader Settings
let mapleader = ","
let g:mapleader = ","

nmap <leader>w :w!<cr>

map <leader>cd :cd %:p:h<cr>:pwd<cr>

nnoremap <leader>ev :vsplit $MYVIMRC<cr>

" Mouse scrolling and selecting
set mouse=a
set noshowmode

" Show line numbers
set nu
set so=7

" Fix backspace errors
set backspace=indent,eol,start

set cmdheight=2
set ignorecase

set mat=2

" Wrap linebreaks with fancy arrow
set wrap linebreak
let &showbreak="\u21aa "

" Remove left and right scroll bars
set guioptions-=r 
set guioptions-=L


" Treat long lines as break lines (useful when moving around in them)
map j gj
map k gk

" Smart way to move between windows
" map <C-j> <C-W>j
" map <C-k> <C-W>k
" map <C-h> <C-W>h
" map <C-l> <C-W>l
map <leader>h <C-W>h
map <leader>j <C-W>j
map <leader>k <C-W>k
map <leader>l jC-W>l

cmap w!! w !sudo tee % >/dev/null


" Code Folding

set foldmethod=indent
set foldlevel=99

nnoremap <space> za

" ===================== Vim-Plug =======================
"

call plug#begin('~/.vim/plugged')

" General
" Scripts
Plug 'airblade/vim-gitgutter'
Plug 'justinmk/vim-sneak'
" Plug 'tpope/vim-commentary'
" Plug 'tpope/vim-surround'
" Plug 'junegunn/goyo.vim'
" Plug 'w0rp/ale'
" Plug 'kien/ctrlp.vim'
" Plug 'djoshea/vim-autoread'
" Plug 'ajh17/VimCompletesMe'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all'}
" Plug 'neoclide/coc.nvim', {'branch': 'release'}
" Plug 'tpope/vim-fugitive'
Plug 'scrooloose/nerdtree', {'on': 'NERDTreeToggle'}
" Plug 'mhinz/vim-startify'

" Theming
Plug 'itchyny/lightline.vim'
Plug 'itchyny/landscape.vim'
Plug 'ryanoasis/vim-devicons'
" Plug 'chriskempson/base16-vim'
" Plug 'dracula/vim', {'as': 'dracula'}
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'nvim-treesitter/playground'
Plug 'neovim/nvim-lspconfig'

" Tmux
Plug 'tmux-plugins/vim-tmux'
Plug 'tmux-plugins/vim-tmux-focus-events'
Plug 'edkolev/tmuxline.vim'

" Python
Plug 'vim-scripts/python.vim', {'for': 'python'}
Plug 'vim-scripts/indentpython.vim', {'for': 'python'}
Plug 'tmhedberg/SimpylFold', {'for': 'python'}

" Rust
Plug 'rust-lang/rust.vim', {'for': 'rust'}

" Docker
Plug 'ekalinin/Dockerfile.vim'

" Yaml

call plug#end()


" =====================    Ale     =======================

" ===================== SimpylFold =======================

let g:SimplyFold_docstring_preview = 1
let g:SimpylFold_fold_import = 0
highlight Folded ctermbg=White ctermfg=Black

" ===================== Airline =======================

set laststatus=2
let g:lightline_powerline_fonts = 1
let g:lightline = {
			\ 'active': {
			\	'right': [ ['lineinfo' ],
			\ 		   [ 'percent' ],
			\		   [ 'gitbranch' ],
			\                  [ 'cocstatus' ]
			\                ]
			\ },
			\ 'component_function': {
			\	'gitbranch': 'fugitive#head',
			\       'cocstatus': 'coc#status'
			\ },
			\ }
let g:lightline.colorscheme = 'landscape'
autocmd User CocStatusChange,CocDiagnosticChange call lightline#update()
" set guifont=FantasqueSansMono\ Nerd\ Font:h14


let g:tmuxline_powerline_separators = 0
let g:tmuxline_preset = 'full'

colorscheme landscape
set background=light
" colorscheme dracula

" ===================== NerdTree =======================

map <leader>t :NERDTreeToggle<CR>

" ===================== Notational =======================
"
" let g:nv_search_paths = ['~/notes']

" nnoremap <silent> <leader>n :NV<CR>

" =====================  ctrl-p  =======================

let g:ctrlp_user_command = 'ag %s -l --nocolor --hidden -g ""'

" =====================  Python  =======================

au BufNewFile,BufRead *.py set
			\ tabstop=4
			\ softtabstop=4
			\ shiftwidth=4
			\ textwidth=79
			\ expandtab
			\ autoindent
			\ fileformat=unix
			\ syntax=python

au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/
highlight BadWhitespace ctermbg=Red ctermfg=White

" =====================  R  =======================

au BufNewFile,BufRead *.R, *.r set
			\ tabstop=4
			\ softtabstop=4
			\ shiftwidth=4
			\ textwidth=79
			\ expandtab
			\ autoindent
			\ fileformat=unix
			\ syntax=R


" Run black on save
" autocmd BufWritePre *.py execute ':Black'

" ===================== Javascript =======================

au BufNewFile,BufRead *.js, *.html, *.css set
			\ tabstop=2
			\ softtabstop=2
			\ shiftwidth=2

" =====================    Rust    =======================

au BufNewFile,BufRead *.rs set
			\ tabstop=4
			\ softtabstop=4
			\ shiftwidth=4
			\ textwidth=100
			\ expandtab
			\ autoindent
			\ fileformat=unix
			\ syntax=rust

let g:rustfmt_autosave = 1

" =====================    JSON    =======================

autocmd FileType json syntax match Comment +\/\/.\+$+

" =====================    YAML    =======================

au! BufNewFile,BufReadPost *.{.yml,yaml} set filetype=yaml foldmethod=indent
autocmd FileType yaml setlocal 
			\ tabstop=2
			\ softtabstop=2
			\ shiftwidth=2
			\ expandtab

" Extra


fun! CleanTrailingSpace()
	let save_cursor = getpos(".")
	let old_query = getreg("/")
	silent %s/\s+$//e
	call setpos(".", save_cursor)
	call setreg("/", old_query)
endfun

if has("autocmd")
	autocmd BufWritePre *.txt, *.py, *.sh :call CleanTrailingSpace()
endif

" END
