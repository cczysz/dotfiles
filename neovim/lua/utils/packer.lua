-- Packer.nvim
-- Bootstrap Packer.nvim if it doesn't exist
local install_path = vim.fn.stdpath('data') ..
                         '/site/pack/packer/opt/packer.nvim'
if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
    vim.api.nvim_command(
        '!git clone https://github.com/wbthomason/packer.nvim ' .. install_path)
    vim.cmd [[:q!]]
end
vim.cmd [[packadd packer.nvim]]
vim.cmd 'autocmd BufWritePost packer.lua PackerCompile'

require('packer').startup({
	function(use)
		use {'wbthomason/packer.nvim', opt = true}
		use {'nvim-treesitter/nvim-treesitter'}
		use {'neovim/nvim-lspconfig'}
		use {'norcalli/snippets.nvim'}
		use {'hrsh7th/nvim-compe'}
		use {
			'nvim-telescope/telescope.nvim',
			requires = {{'nvim-lua/popup.nvim'}, {'nvim-lua/plenary.nvim'}}
		}
		
		-- Themes
		use {
			'glepnir/galaxyline.nvim',
			requires = {'kyazdani42/nvim-web-devicons', opt = true}
		}
		use {'dracula/vim'}
		use {'tanvirtin/monokai.nvim'}
	end,
	config = {display = {open_fn = require"packer.util".float}}
})
