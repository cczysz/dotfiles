local cmd = vim.cmd
local fn = vim.fn
local g = vim.g
local o, wo, bo = vim.o, vim.wo, vim.bo

require('utils.packer')
require('utils.lsp')
require('plugins.treesitter')
require('plugins.compe')
require('plugins.snippets')
require('plugins.galaxyline')
require('plugins.telescope')
cmd [[colorscheme monokai]]

-- Config
o.termguicolors = true
cmd 'syntax enable'
cmd 'filetype plugin indent on'
g.mapleader = ','
o.clipboard = "unnamedplus"
o.expandtab = true
o.ignorecase = true
o.hidden = true
o.showtabline = 2
o.splitbelow = true
o.splitright = true
o.title = true

o.tabstop = 2
o.softtabstop = 2
o.smartindent = true
o.autoindent = true

wo.number = true
o.swapfile = false
o.backup = false

local opt = {noremap = true, silent = true}

-- mappings
vim.api.nvim_set_keymap("n", "<Leader>ff", [[<Cmd>lua require('telescope.builtin').find_files()<CR>]], opt)
